generate:
	@test "x" != "x$(NAMESPACE)"
	@rsync --archive --human-readable --verbose --delete templates/namespace/ namespaces/$(NAMESPACE)/

resync:
	@for namespace in namespaces/*; do \
		echo "> Namespace $${namespace##*/} resyncing" ; \
		NAMESPACE="$${namespace##*/}" $(MAKE) -s generate ; \
	done
