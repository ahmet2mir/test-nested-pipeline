#!/bin/bash

# https://gitlab.com/gitlab-org/project-templates/jsonnet/-/tree/master

service=$1
shift
namespaces=( "$@" )

echo "$service"

cat>generated-service-${service}.yml<<EOF
stages:
  - test_${service}
  - build_${service}
  - deploy_${service}
EOF

for namespace in "$@"
do
  cat>>generated-service-${service}.yml<<EOF
test-${namespace}-${service}:
  stage: test_${service}
  image: "alpine:latest"
  script:
    - echo "test ${namespace}"

build-${namespace}-${service}:
  stage: build_${service}
  image: "alpine:latest"
  needs: [test-${namespace}-${service}]
  script:
    - echo "build ${namespace}"

deploy-${namespace}-${service}:
  stage: deploy_${service}
  image: "alpine:latest"
  needs: [build-${namespace}-${service}]
  rules:
    - when: on_success
      when: manual
  script:
    - echo "deploy ${namespace}-${service}"
EOF
done
