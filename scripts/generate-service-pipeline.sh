#!/bin/bash

# https://gitlab.com/gitlab-org/project-templates/jsonnet/-/tree/master

cat>generated-services.yml<<EOF
stages:
  - test
  - build
  - deploy
  - ipsum
EOF

echo "CI_COMMIT_BEFORE_SHA=${CI_COMMIT_BEFORE_SHA}"
echo "CI_COMMIT_SHA=${CI_COMMIT_SHA}"


declare -a services
declare -a namespace_services

containsElement () {
  local e match="$1"
  shift
  for e; do [[ "$e" == "$match" ]] && return 0; done
  return 1
}
for namespace_service in $(git diff --name-only "${CI_COMMIT_BEFORE_SHA}" "${CI_COMMIT_SHA}" --diff-filter=ACM 2>/dev/null | grep '^namespaces/' | sed 's/^namespaces\///g' | cut -d '/' -f1,2 | sort | uniq)
do
    if ! containsElement "${namespace_service##*/}" "${services[@]}"
    then
      services+=("${namespace_service##*/}")
    fi
    namespace_services+=("${namespace_service}")
done

echo "services=${services[@]}"
echo "namespace_services=${namespace_services[@]}"

for service in "${services[@]}"
do
  namespaces=()
  for namespace_service in "${namespace_services[@]}"
  do
    namespace="${namespace_service%%/*}"
    _service="${namespace_service##*/}"
    [[ "x${service}" = "x${_service}" ]] && namespaces+=("${namespace}")
  done
  echo "service=${_service} namespaces=${namespaces[@]}"

    cat>>generated-services.yml<<EOF

${service}:
  stage: build
  image: "alpine:latest"
  script:
    - apk add --update git bash
    - ./scripts/generate-namespace-pipeline.sh ${service} ${namespaces[@]}
  artifacts:
    paths:
      - generated-service-${service}.yml

${service}-trigger:
  stage: deploy
  needs:
    - ${service}
  trigger:
    include:
      - artifact: generated-service-${service}.yml
        job: ${service}
    strategy: depend

EOF
done
